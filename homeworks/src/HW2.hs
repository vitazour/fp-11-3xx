-- Тесты чуть позже

module HW2
       ( Contact (..)
       , isKnown
       , Term (..)
       , eval
       , simplify
       ) where

data Contact = On
             | Off
             | Unknown

isKnown :: Contact -> Bool
isKnown Unknown = False
isKnown _ = True

data Term = Mult Term Term      -- умножение
          | Add Term Term       -- сложение
          | Sub Term Term       -- вычитание
          | Const Int           -- константа

eval :: Term -> Int
eval (Const number) = number
eval (Add term1 term2) = eval term1 + eval term2
eval (Sub term1 term2) = eval term1 - eval term2
eval (Mult term1 term2) = eval term1 * eval term2

-- Раскрыть скобки
-- Mult (Add (Const 1) (Const 2)) (Const 3) ->
-- Add (Mult (Const 1) (Const 3)) (Mult (Const 2) (Const 3))
-- (1+2)*3 -> 1*3+2*3
simplify :: Term -> Term
simplify (Add term1 (Add term2_1 term2_2)) = simplify $ Add (Add (term1) (term2_1)) (term2_2)
simplify (Add term1 (Sub term2_1 term2_2)) = simplify $ Sub (Add (term1) (term2_1)) (term2_2)
simplify (Add term1 term2) = Add (simplify term1) (simplify term2)

simplify (Sub term1 (Add term2_1 term2_2)) = simplify $ Sub (Sub (term1) (term2_1)) (term2_2)
simplify (Sub term1 (Sub term2_1 term2_2)) = simplify $ Add (Sub (term1) (term2_1)) (term2_2)
simplify (Sub term1 term2) = Sub (simplify term1) (simplify term2)

simplify (Mult term1 (Mult term2_1 term2_2)) = simplify $ Mult (Mult (term1) (term2_1)) (term2_2)
simplify (Mult term1 (Add term2_1 term2_2)) = simplify $ Add (Mult (term1) (term2_1)) (Mult (term1) (term2_2))
simplify (Mult (Add term2_1 term2_2) term1) = simplify $ Add (Mult (term2_1) (term1)) (Mult (term2_2) (term1))
simplify (Mult term1 (Sub term2_1 term2_2)) = simplify $ Sub (Mult (term1) (term2_1)) (Mult (term1) (term2_2))
simplify (Mult (Sub term2_1 term2_2) term1) = simplify $ Sub (Mult (term2_1) (term1)) (Mult (term2_2) (term1))
simplify (Mult term1 term2) = Mult (simplify term1) (simplify term2)

simplify (Const num) = Const num

