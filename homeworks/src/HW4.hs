-- Срок: 2016-04-02 (100%), 2016-04-07 (50%)

module HW4
       ( Show' (..)
       , A(..)
       , C(..)
       , Set(..)
       , symmetricDifference
       , fromBool
       , fromInt
       ) where

class Show' a where
  show' :: a -> String

data A = A Int
       | B
-- show' (A 5)    == "A 5"
-- show' (A (-5)) == "A (-5)"
-- show' B        == "B"

data C = C Int Int
-- show' (C 1 2)       == "C 1 2"
-- show' (C (-1) (-2)) == "C (-1) (-2)"

instance Show' A where

  show' (A a) = if a>=0 then "A " ++ (show a) else "A ("++show(a)++")"
  show' B = "B"
instance Show' C where
  show' (C a b) = if (a>0)&&(b>0) then "C " ++ (show a) ++" "++ (show b)
                  else if(a<0)&&(b>0) then "C (" ++ (show a) ++ ")"++(show b)
                        else if (a>0)&&(b<0) then "C " ++ (show a) ++"("++ (show b)++")"
                             else "C (" ++ (show a) ++ ") ("++ (show b)++")"

----------------

data Set a = Set (a -> Bool)

-- Симметрическая разность -
-- элементы обоих множеств, не входящие в объединение
-- {4,5,6} \/ {5,6,7,8} == {4,7,8}
symmetricDifference :: Set a -> Set a -> Set a
symmetricDifference (Set f) (Set g) = Set $ \x -> (f x || g x) && not(f x && g x)

-----------------

-- fromBool - переводит булевское значение в кодировку Чёрча
fromBool True = \t -> (\f -> t)
fromBool False = \t f -> f

-- fromInt - переводит число в кодировку Чёрча
fromInt a = if a>0 then \s z -> s (fromInt (a-1) s z) else \s z -> z

