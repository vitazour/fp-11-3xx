-- ��������� ������. ����� ���� ����������, ������� �� ����������, ������������ ���� ��� ����������� ����� 
data Term = Variable String
          | Lambda String Term
          | Apply Term Term


-- ���������� ������ Show ��� ���� Term. ��������� �������� ����
instance Show Term where
    show (Variable x) = x
    show (Apply term1 term2) = (show term1) ++ " " ++ (show term2)
    show (Lambda var res) = "(\\" ++ var ++ "." ++ (show res) ++ ")" 

-- �������� ���������� ������, ������� ��� � ������
renameVariable :: [String] -> String -> Int -> String
renameVariable lst x n | elem (x ++ (show n)) lst = renameVariable lst x (n+1)
                       | otherwise = x ++ (show n)

-- ��������������� ��� ����������, ����� ������� ��������� � ����., ��� ���� � ������
renameFreeVariables :: [String] -> Term -> Term
renameFreeVariables lst (Lambda x term) | elem x lst = Lambda x (renameFreeVariables (filter (\el -> el /= x) lst) term)
                                        | otherwise = Lambda x (renameFreeVariables lst term)
renameFreeVariables lst (Apply t1 t2) = Apply (renameFreeVariables lst t1) (renameFreeVariables lst t2)
renameFreeVariables lst (Variable x) | elem x lst = Variable (renameVariable lst x 0)
                                     | otherwise = Variable x


-- �������, ���������� ������ ���������� �� ������ ���� 
setVariable :: String -> [String] -> Term -> Term -> Term

setVariable var lst (Lambda x term) needTerm | x /= var = Lambda x (setVariable var (x:lst) term needTerm)
                                             | otherwise = Lambda x term
setVariable var lst (Apply term1 term2) needTerm = Apply (setVariable var lst term1 needTerm) (setVariable var lst term2 needTerm)
setVariable var lst (Variable x) needTerm | x == var = renameFreeVariables lst needTerm
                                          | otherwise = Variable x  

-- �������, �������� ���� ��� � ������������ �����
eval1 :: Term -> Term
eval1 (Apply (Lambda var res) t2) = setVariable var [] res t2
eval1 (Apply (Apply term1 term2) t2) = Apply (eval1 (Apply term1 term2)) t2
eval1 term = term

id' = Lambda "x" (Variable "x")
termList = [Apply id' (Apply id' (Lambda "z" (Apply id' (Variable "z")))),
                 Apply (Lambda "x" id') (Variable "x"),
                 Apply (Apply (Lambda "x" (Lambda "y" (Apply (Variable "x") (Variable "y")))) (Lambda "z" (Variable "z"))) (Lambda "u" (Lambda "v" (Variable "v")))]

-- �������, �������� ������������ ����� ����� � ������������ �����
eval :: Term -> Term
eval (Apply (Lambda var res) t2) = eval (setVariable var [] res t2)
eval (Apply (Apply term1 term2) t2) = eval (Apply (eval (Apply term1 term2)) t2)
eval term = term

-- (\y . (\z . z) y) (\z . z) (\u . \v . v)
-- Apply (Lambda 'x' $ Lambda 'y' $ Variable 'x') (Lambda 'y' $ Variable 'y')